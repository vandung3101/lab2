# To do
# Secure VM with network security group
# Hide credentials information from plan text 
# data 
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
    backend "azurerm" {
        resource_group_name  = "Default_Sea"
        storage_account_name = "terrastoacc5"
        container_name       = "tfstate"
        key                  = "terraform.tfstate"
    }

}

provider "azurerm" {
  features {}
}

data "azurerm_key_vault" "keyvault" {
  name                = "${var.keyvault_name}"
  resource_group_name = "${var.keyvault_location}"
}

data "azurerm_key_vault_secret" "vm-pass" {
  name         = "vm-pass"
  key_vault_id = data.azurerm_key_vault.keyvault.id
}

data "azurerm_key_vault_secret" "vm-username" {
  name         = "vm-username"
  key_vault_id = data.azurerm_key_vault.keyvault.id
}

data "azurerm_key_vault_secret" "ssh-pub-key" {
  name         = "ssh-pub-key"
  key_vault_id = data.azurerm_key_vault.keyvault.id
}

resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-resources"
  location = var.location
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "public_ip" {
  count               = 2
  name                = "public-ip-${count.index}"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "nic" {
  count               = 2
  name                = "nic-${count.index}"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = element(azurerm_public_ip.public_ip.*.id, count.index)
  }
}

resource "azurerm_linux_virtual_machine" "linuxvm" {
  name                = "${var.prefix}-linuxvm"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_F2"
  admin_username      = "${data.azurerm_key_vault_secret.vm-username.value}"
  network_interface_ids = [
    azurerm_network_interface.nic[0].id,
  ]

  admin_ssh_key {
    username   = "${data.azurerm_key_vault_secret.vm-username.value}"
    public_key = "${data.azurerm_key_vault_secret.ssh-pub-key.value}"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }


  provisioner "local-exec" {
    command = <<EOT
        echo "linux ansible_host=${azurerm_linux_virtual_machine.linuxvm.public_ip_address} ansible_connection=ssh ansible_user=${data.azurerm_key_vault_secret.vm-username.value} ansible_ssh_common_args='-o StrictHostKeyChecking=no'" > inventory
        ansible-playbook -i inventory ansible.yaml

    EOT
  }

  # connection {
  #   host        = azurerm_linux_virtual_machine.linuxvm.public_ip_address
  #   type        = "ssh"
  #   user        = "adminuser"
  #   private_key = file("~/.ssh/id_rsa")
  # }

  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo java -version",
  #   ]
  # }
}

resource "azurerm_windows_virtual_machine" "windownvm" {
  name                = "${var.prefix}-windownvm"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_F2"
  admin_username      = "${data.azurerm_key_vault_secret.vm-username.value}"
  admin_password      = "${data.azurerm_key_vault_secret.vm-pass.value}"
  network_interface_ids = [
    azurerm_network_interface.nic[1].id,
  ]

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}
resource "azurerm_virtual_machine_extension" "winrm" {
  name                 = "ansible_windows_winrm"
  virtual_machine_id   = azurerm_windows_virtual_machine.windownvm.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"

  settings = <<SETTINGS
    {
        "fileUris": ["https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"],
        "commandToExecute": "powershell -ExecutionPolicy Unrestricted -file ConfigureRemotingForAnsible.ps1 -EnableCredSSP -DisableBasicAuth"
    }
SETTINGS
  provisioner "local-exec" {
    command = <<EOT
        echo "window ansible_host=${azurerm_windows_virtual_machine.windownvm.public_ip_address} ansible_connection=winrm ansible_ssh_port=5986 ansible_winrm_transport=ntlm ansible_user=${data.azurerm_key_vault_secret.vm-username.value} ansible_password=${data.azurerm_key_vault_secret.vm-pass.value} ansible_winrm_server_cert_validation=ignore" > inventory
        ansible-playbook -i inventory ansible.yaml
        rm inventory
    EOT
  }

  # connection {
  #   type = "winrm"
  #   port = 5986
  #   user = "adminuser"
  #   password = "P@ssw0rd1234!"
  #   host = "${azurerm_windows_virtual_machine.windownvm.public_ip_address}"
  #   insecure = true
  #   use_ntlm = true
  #   cacert = ""
  # }

  # provisioner "remote-exec" {
  #   inline = [
  #     "java -version",
  #   ]
  # }
}

# resource "null_resource" "run-ansible" {
#   provisioner "local-exec" {
#     command = <<EOT
#         echo "linux ansible_host=${azurerm_linux_virtual_machine.linuxvm.public_ip_address} ansible_connection=ssh ansible_user=adminuser ansible_ssh_common_args='-o StrictHostKeyChecking=no'" > inventory
#         echo "window ansible_host=${azurerm_windows_virtual_machine.windownvm.public_ip_address} ansible_connection=winrm ansible_ssh_port=5986 ansible_winrm_transport=ntlm ansible_user=adminuser ansible_password=P@ssw0rd1234! ansible_winrm_server_cert_validation=ignore" >> inventory
#         ansible-playbook -i inventory ansible.yaml
#     EOT
#   }
# }