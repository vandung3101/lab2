variable "prefix" {
  default     = "week5"
  description = "The prefix which should be used for all resources in this example"
}

variable "location" {
  default     = "westeurope"
  description = "The Azure Region in which all resources in this example should be created."
}

variable "keyvault_name" {
  default = "terra5"
  description = "The key vault contains secret information use for terraform"
}

variable "keyvault_location" {
  default = "Default_Sea"
}

# variable "state_resource_group_name" {
#   default = "Default_Sea"
# }

# variable "state_storage_account_name" {
#   default = "terrastoacc5"
# }

# variable "container_name" {
#   default = "tfstate"
# }