output "linuxvm-publicip" {
  value = azurerm_linux_virtual_machine.linuxvm.public_ip_address
}

output "winvm-publicip" {
  value = azurerm_windows_virtual_machine.windownvm.public_ip_address
}
  